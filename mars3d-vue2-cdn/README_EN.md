<p align="center">
<img src="https://mars3d.cn/logo.png" width="300px" />
</p>

<p align="center">Mars3d development template based on Vue template (Introduced by CDN)</p>

<p align="center">
<a target="_black" href="https://github.com/marsgis/mars3d">
<img alt="GitHub stars" src="https://img.shields.io/github/stars/marsgis/mars3d?style=flat&logo=github">
</a>
<a target="_black" href="https://www.npmjs.com/package/mars3d">
<img alt="Npm downloads" src="https://img.shields.io/npm/dt/mars3d?style=flat&logo=npm">
</a>
<a target="_black" href="https://www.npmjs.com/package/mars3d">
<img alt="Npm version" src="https://img.shields.io/npm/v/mars3d.svg?style=flat&logo=npm&label=version"/>
</a>
</p>

 [**English**](./README_EN.md) |[**中文**](./README.md) 

 
 
 
  
## Run the command
 
### Install dependencies before first run
 `npm install` or `cnpm install`
 
### http run project
 `npm run serve`  after run access：`http://localhost:3005/` 

### Package and compile project
 Run `npm run build` to build the project.  

## Operation effect  
 [online Demo](http://mars3d.cn/project/vue-template/)  

 ![image](http://mars3d.cn/project/vue-template/screenshot.jpg)
 

  
## How to integrate into your existing projects
1. ### Download the latest Cesium Library
 Please pass [http://mars3d.cn/download](http://mars3d.cn/download) Download the latest SDK
 
2. ### Copy files
 > Scene profile：`public\config\config.json` 

 > Component definition file：`src\components\mars3d\Map.vue`
 
3. ### Create the earth 
 Refer to the `src\views\Index.vue` file to introduce the Map component and construct the creation of the earth, focusing on the following code
```js
<Map :url="configUrl" @onload="onMapload" />

import Map from '../components/mars3d/Map.vue'
```

4. ### If you want to use mars3d more gracefully with NPM, you can switch to the following template for use
 Simplest project(NPM version)[mars3d-vue2](../mars3d-vue2/) `npm introduces Cesium and mars3d`


## What is Mars3D 
>  `Mars3D platform` is [Mars technology](http://marsgis.cn/) a 3D client development platform based on WebGL technology, which is based on [Cesium](https://cesium.com/cesiumjs/) optimization and B / S architecture design,The lightweight and efficient GIS development platform supporting multi industry expansion can run efficiently in the browser without installation and plug-ins, and can quickly access and use a variety of GIS data and three-dimensional models, present the visualization of three-dimensional space, and complete the flexible application of the platform in different industries.

 > Mars3d platform can be used to build 3D GIS applications without plug-ins, across operating systems and across browsers. The platform uses WebGL for hardware accelerated graphics, and realizes real dynamic big data 3D visualization across platforms and browsers. The Mars3D product can quickly realize beautiful and smooth 3D map presentation and spatial analysis on browsers and mobile terminals.

### Related websites 
- Mars3D official website：[http://mars3d.cn](http://mars3d.cn)  

- Mars3D GitHub navigation list：[https://github.com/marsgis/mars3d](https://github.com/marsgis/mars3d)




