<p align="center">
<img src="https://mars3d.cn/logo.png" width="300px" />
</p>

<p align="center">Mars3d development template based on Vue2.x</p>

<p align="center">
<a target="_black" href="https://github.com/marsgis/mars3d">
<img alt="GitHub stars" src="https://img.shields.io/github/stars/marsgis/mars3d?style=flat&logo=github">
</a>
<a target="_black" href="https://www.npmjs.com/package/mars3d">
<img alt="Npm downloads" src="https://img.shields.io/npm/dt/mars3d?style=flat&logo=npm">
</a>
<a target="_black" href="https://www.npmjs.com/package/mars3d">
<img alt="Npm version" src="https://img.shields.io/npm/v/mars3d.svg?style=flat&logo=npm&label=version"/>
</a>
</p>

 [**English**](./README_EN.md) |[**中文**](./README.md) 
 

 
  
| Directory  |Mars3D |Cesium  |   Instructions  | 
|  ----  |----  | ----| ----  |
|[mars3d-vue2](./mars3d-vue2/README.md)	|npm |npm  | [Recommended] Standard vue2 project template| 
|[mars3d-vue2-cdn](./mars3d-vue2-cdn/README.md)	|cdn |cdn  | Integration methods introduced using CDN | 
|[mars3d-vue2-local](./mars3d-vue2-local/README.md)	|The local file |npm  | Mars3d licensed version of the integration method was purchased| 
|[mars3d-vue2-supermap](./mars3d-vue2-supermap/README.md)		|npm |cdn  | Project template for SuperMap| 
|[mars3d-vue2-electron](./mars3d-vue2-electron/README.md)		|npm |npm  | A project template for CS desktop applications| 

 

## Vue3
   Mars3D platform ， different application project templates under `Vue 2.x` technology stack。For `Vue 3.x` technology stack development, please refer to[mars3d-vue-template](https://github.com/marsgis/mars3d-vue-template)
   

## Operation effect 
 [online Demo](http://mars3d.cn/project/vue-template/)  

 ![image](http://mars3d.cn/project/vue-template/screenshot.jpg)
  
  

## What is Mars3D
>  `Mars3D platform` is [Mars technology](http://marsgis.cn/) a 3D client development platform based on WebGL technology, which is based on [Cesium](https://cesium.com/cesiumjs/) optimization and B / S architecture design,The lightweight and efficient GIS development platform supporting multi industry expansion can run efficiently in the browser without installation and plug-ins, and can quickly access and use a variety of GIS data and three-dimensional models, present the visualization of three-dimensional space, and complete the flexible application of the platform in different industries.

 > Mars3d platform can be used to build 3D GIS applications without plug-ins, across operating systems and across browsers. The platform uses WebGL for hardware accelerated graphics, and realizes real dynamic big data 3D visualization across platforms and browsers. The Mars3D product can quickly realize beautiful and smooth 3D map presentation and spatial analysis on browsers and mobile terminals.

### Related websites
- Mars3D official website: [http://mars3d.cn](http://mars3d.cn)

- Making navigation list: [https://github.com/marsgis/mars3d](https://github.com/marsgis/mars3d)
 
 


